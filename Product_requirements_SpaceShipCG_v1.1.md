## Requirements for the project outcome

The final result should be that a player can fly the ship in VR and switch between camera views while flying (FPV, ground cam, air cam).

### Tech
* VR: Oculus Rift with Touch controllers or xbox controls
* UE4
* Blender

### Spaceship
* Cool enough to make creators happy and final players smile :)
* Size is either a 1-seater or 2-seater 
* Detail level simple enough for not spending too much time on building it
* Curved and rectangular surfaces both present for different light reflection visuals
* Some small lights on the ship so it will be visible in low light conditions
* For inspiration, some favorites and ideas for the style or overall shape (but not for details, because they are too complex):
 - X Wing
 - Normandy SR2
 - Other links from our first project meeting:
   https://www.pixel-creation.com/wp-content/uploads/x-wing-wallpaper-hd-62-images.jpg
   https://www.wallpaperflare.com/static/1002/424/1014/star-wars-lego-star-wars-x-wing-spaceship-wallpaper.jpg
   https://www.tokkoro.com/picsup/1405822-spaceship.jpg
   https://a-static.besthdwallpaper.com/arena-commander-behang-3840x2160-3314_54.jpg

### World
* At least 1 light source
* Cameras in at least 2 places for different viewing angles 
 - 1 - looking from the ground at the ship
 - 2 - looking from another ship flying next to it
* Can be some planet surface and space above it
* Fog or clouds (for different light effects)
* Sunbeams shining through cloud and reflecting from the ship's surface
* Some buildings are ok but most important is landscape and water
* Examples:
 - https://www.wallpaperflare.com/static/1002/424/1014/star-wars-lego-star-wars-x-wing-spaceship-wallpaper.jpg
