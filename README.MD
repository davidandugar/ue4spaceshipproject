## Welcome to our VR Spaceship project !

We have created a spaceship that you can control and see some cool light effects with it. We also created a nice world where the spaceship can fly around in VR using the Xbox controllers.

## How to launch this project?

1) Download the project

2) Launch Unreal Engine 4.23.1

3) Click the browse option in the bottom right corner of the project browser

4) Navigate to the folder of the project and select the `SpaceShipCG.uproject`

5) Click open to launch the project

6) To run the project, press `Play` from the bar on top

## Spaceship controls

* Thrust forward: Left Shift

* Thrust backwards: Left Ctrl

* Turn right: E

* Turn left: Q

* Rotate right: D

* Rotate left: A

* Look up: S

* Look down: W

* Change Camera: C

* Retract Landing Gear: G